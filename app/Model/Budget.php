<?php
App::uses('AppModel', 'Model');
/**
 * Budget Model
 *
 * @property Bill $Bill
 */
class Budget extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Bill' => array(
			'className' => 'Bill',
			'foreignKey' => 'budget_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
