<?php

     App::uses('AppModel', 'Model');

     /**
      * User Model
      *
      * @property Role $Role
      * @property Account $Account
      */
     class User extends AppModel {

         public $name = 'User';

         /**
          * Validation rules
          *
          * @var array
          */
         public $validate = array(
                 'role_id' => array(
                         'numeric' => array(
                                 'rule' => array('numeric'),
                                 'message' => 'just Letters and numbers!!',
                                 'allowEmpty' => false,
                                 'required' => true,
                         //'last' => false, // Stop validation after this rule
                         //'on' => 'create', // Limit validation to 'create' or 'update' operations
                         ),
                 ),
                 'username' => array(
                         'notEmpty' => array(
                                 'rule' => array('notEmpty'),
                                 'alphaNumeric' => array(
                                    'rule' => 'alphaNumeric' ,
                                    'required' => true,
                                    'message' => 'Letters and numbers only'
                                    ),
                                 'allowEmpty' => false,
                                 'required' => true,
                         //'last' => false, // Stop validation after this rule
                         //'on' => 'create', // Limit validation to 'create' or 'update' operations
                         ),
                         'unique' => array(
                                 'rule' => 'isUnique',
                                 'required' => 'create',
                                 'message'=>'Já existe!!'
                         ),
                 ),
                 'password' => array(
                        
                         'notEmpty' => array(
                                 'rule' => array('notEmpty'),
                                 'message' => 'Your custom message here',
                                 'allowEmpty' => false,
                                 'required' => true,
                         //'last' => false, // Stop validation after this rule
                         //'on' => 'create', // Limit validation to 'create' or 'update' operations
                         ),
                         'between' => array(
                                 'rule' => array('between', 5, 15),
                                 'message' => 'Between 5 to 15 characters'
                         )
                 ),
                 'status' => array(
                         'boolean' => array(
                                 'rule' => array('boolean'),
                                 'message' => 'Your custom message here',
                                 'allowEmpty' => false,
                                 'required' => true,
                         //'last' => false, // Stop validation after this rule
                         //'on' => 'create', // Limit validation to 'create' or 'update' operations
                         ),
                 ),
         );

         //The Associations below have been created with all possible keys, those that are not needed can be removed

         /**
          * belongsTo associations
          *
          * @var array
          */
         public $belongsTo = array(
                 'Role' => array(
                         'className' => 'Role',
                         'foreignKey' => 'role_id',
                         'conditions' => '',
                         'fields' => '',
                         'order' => ''
                 )
         );

         /**
          * hasMany associations
          *
          * @var array
          */
         public $hasMany = array(
                 'Account' => array(
                         'className' => 'Account',
                         'foreignKey' => 'user_id',
                         'dependent' => false,
                         'conditions' => '',
                         'fields' => '',
                         'order' => '',
                         'limit' => '',
                         'offset' => '',
                         'exclusive' => '',
                         'finderQuery' => '',
                         'counterQuery' => ''
                 )
         );

         public function beforeSave($options = array()) {
             if (!empty($this->data['User']['password'])) {
                 $this->data['User']['password'] = AuthComponent::password($this->data['User']['password']);
             }
             return true;
         }

     }
     