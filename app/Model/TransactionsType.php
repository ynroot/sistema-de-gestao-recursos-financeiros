<?php
App::uses('AppModel', 'Model');
/**
 * TransactionsType Model
 *
 * @property Transaction $Transaction
 */
class TransactionsType extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'title' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				'alphaNumeric' => array(
                                    'rule' => 'alphaNumeric' ,
                                    'required' => true,
                                    'message' => 'Letters and numbers only'
                                    ),
				
				'allowEmpty' => false,
				'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
                        'unique' => array(
                                 'rule' => 'isUnique',
                                 'required' => 'create',
                                 'message' => 'Já existe !!!'
                         ),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Transaction' => array(
			'className' => 'Transaction',
			'foreignKey' => 'transactions_type_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
