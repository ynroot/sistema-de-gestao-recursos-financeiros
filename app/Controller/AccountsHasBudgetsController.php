<?php
App::uses('AppController', 'Controller');
/**
 * AccountsHasBudgets Controller
 *
 * @property AccountsHasBudget $AccountsHasBudget
 * @property PaginatorComponent $Paginator
 */
class AccountsHasBudgetsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->AccountsHasBudget->recursive = 0;
		$this->set('accountsHasBudgets', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->AccountsHasBudget->exists($id)) {
			throw new NotFoundException(__('Invalid accounts has budget'));
		}
		$options = array('conditions' => array('AccountsHasBudget.' . $this->AccountsHasBudget->primaryKey => $id));
		$this->set('accountsHasBudget', $this->AccountsHasBudget->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->AccountsHasBudget->create();
			if ($this->AccountsHasBudget->save($this->request->data)) {
				$this->Session->setFlash(__('The accounts has budget has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The accounts has budget could not be saved. Please, try again.'), 'flash/error');
			}
		}
		$accounts = $this->AccountsHasBudget->Account->find('list');
		$budgets = $this->AccountsHasBudget->Budget->find('list');
		$this->set(compact('accounts', 'budgets'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
        $this->AccountsHasBudget->id = $id;
		if (!$this->AccountsHasBudget->exists($id)) {
			throw new NotFoundException(__('Invalid accounts has budget'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->AccountsHasBudget->save($this->request->data)) {
				$this->Session->setFlash(__('The accounts has budget has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The accounts has budget could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('AccountsHasBudget.' . $this->AccountsHasBudget->primaryKey => $id));
			$this->request->data = $this->AccountsHasBudget->find('first', $options);
		}
		$accounts = $this->AccountsHasBudget->Account->find('list');
		$budgets = $this->AccountsHasBudget->Budget->find('list');
		$this->set(compact('accounts', 'budgets'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->AccountsHasBudget->id = $id;
		if (!$this->AccountsHasBudget->exists()) {
			throw new NotFoundException(__('Invalid accounts has budget'));
		}
		if ($this->AccountsHasBudget->delete()) {
			$this->Session->setFlash(__('Accounts has budget deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Accounts has budget was not deleted'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}
}
