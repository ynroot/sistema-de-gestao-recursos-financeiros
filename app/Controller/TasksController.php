<?php
App::uses('AppController', 'Controller');
/**
 * Tasks Controller
 *
 * @property Task $Task
 * @property PaginatorComponent $Paginator
 */
class TasksController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
//	public function index() {
//		$this->Task->recursive = 0;
//		$this->set('tasks', $this->paginate());
//	}

	public function index($id = null) {
		$this->loadModel('Account');
		if (!$this->Account->exists($id)) {
			throw new NotFoundException(__('Invalid restaurant'));
		}

		$this->paginate = array(
			'conditions' => array('Task.account_id' => $id)
		);

		$this->Task->recursive = 0;
		$this->set('tasks', $this->Paginator->paginate());

		/**
		 * igual a view method
		 *
		 * @throws NotFoundException
		 * @param string $id
		 * @return restaurant name
		 */
		//$options = array('conditions' => array('Account.'.$this->Task->primaryKey => $id), 'limit'=>1);
		//$this->set('task1', $this->Restaurant->find('first', $options));

	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Task->exists($id)) {
			throw new NotFoundException(__('Invalid task'));
		}
		$options = array('conditions' => array('Task.' . $this->Task->primaryKey => $id));
		$this->set('task', $this->Task->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Task->create();
			if ($this->Task->save($this->request->data)) {
				$this->Session->setFlash(__('The task has been saved'), 'flash/success');
				$this->redirect(array('controller'=>'tasks','action' => 'index',$this->request->data['Task']['back1']));
			} else {
				$this->Session->setFlash(__('The task could not be saved. Please, try again.'), 'flash/error');
			}
		}
		$accounts = $this->Task->Account->find('list');
		$this->set(compact('accounts'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
        $this->Task->id = $id;
		if (!$this->Task->exists($id)) {
			throw new NotFoundException(__('Invalid task'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Task->save($this->request->data)) {
				$this->Session->setFlash(__('The task has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The task could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Task.' . $this->Task->primaryKey => $id));
			$this->request->data = $this->Task->find('first', $options);
		}
		$accounts = $this->Task->Account->find('list');
		$this->set(compact('accounts'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Task->id = $id;
		if (!$this->Task->exists()) {
			throw new NotFoundException(__('Invalid task'));
		}
		if ($this->Task->delete()) {
			$this->Session->setFlash(__('Task deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Task was not deleted'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}
}
