<?php
App::uses('AppController', 'Controller');
/**
 * CategoriesTypes Controller
 *
 * @property CategoriesType $CategoriesType
 * @property PaginatorComponent $Paginator
 */
class CategoriesTypesController extends AppController {
  public $theme = 'CakeAdminLTE';
/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->CategoriesType->recursive = 0;
		$this->set('categoriesTypes', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->CategoriesType->exists($id)) {
			throw new NotFoundException(__('Invalid categories type'));
		}
		$options = array('conditions' => array('CategoriesType.' . $this->CategoriesType->primaryKey => $id));
		$this->set('categoriesType', $this->CategoriesType->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->CategoriesType->create();
			if ($this->CategoriesType->save($this->request->data)) {
				$this->Session->setFlash(__('The categories type has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The categories type could not be saved. Please, try again.'), 'flash/error');
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
        $this->CategoriesType->id = $id;
		if (!$this->CategoriesType->exists($id)) {
			throw new NotFoundException(__('Invalid categories type'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->CategoriesType->save($this->request->data)) {
				$this->Session->setFlash(__('The categories type has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The categories type could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('CategoriesType.' . $this->CategoriesType->primaryKey => $id));
			$this->request->data = $this->CategoriesType->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->CategoriesType->id = $id;
		if (!$this->CategoriesType->exists()) {
			throw new NotFoundException(__('Invalid categories type'));
		}
		if ($this->CategoriesType->delete()) {
			$this->Session->setFlash(__('Categories type deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Categories type was not deleted'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}
}
