<?php

     /**
      * Application level Controller
      *
      * This file is application-wide controller file. You can put all
      * application-wide controller-related methods here.
      *
      * PHP 5
      *
      * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
      * Copyrig\ht 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
      *
      * Licensed under The MIT License
      * Redistributions of files must retain the above copyright notice.
      *
      * @copyright     Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
      * @link          http://cakephp.org CakePHP(tm) Project
      * @package       app.Controller
      * @since         CakePHP(tm) v 0.2.9
      * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
      */
     App::uses('Controller', 'Controller');

     /**
      * Application Controller
      *
      * Add your application-wide methods in the class below, your controllers
      * will inherit them.
      *
      * @package       app.Controller
      * @link http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
      */
     class AppController extends Controller {

         public $components = array(
                 'Auth',
                 'Session',
         );
         public $helpers = array('Html', 'Form', 'Session', 'Number');

        

         public function beforeFilter() {
 $this->theme = 'CakeAdminLTE';
             $this->Auth->authenticate = array(
                     AuthComponent::ALL => array(
                             'userModel' => 'User',
                             'fields' => array(
                                     'username' => 'username',
                             ),
                             'scope' => array(
                                     'User.status' => 1,
                             ),
                     ),
                     'Form',
             );
             $this->Auth->authorize = 'Controller';

             $this->Auth->loginAction = array(
                     'plugin' => null,
                     'controller' => 'users',
                     'action' => 'login',
             );

             $this->Auth->logoutRedirect = array(
                     'plugin' => null,
                     'controller' => 'users',
                     'action' => 'login',
             );

             $this->Auth->loginRedirect = array(
                     'plugin' => null,
                     'controller' => 'users',
                     'action' => 'home'
             );

             $this->Auth->authError = __('Você não possui autorização para executar esta ação.');

             $this->Auth->allowedActions = array('login');
         
         }

         public function isAuthorized($user) {
            
             if (!empty($this->request->params['admin'])) {
                 return $user['role_id'] == 1;
             }
             return !empty($user);
         }

     }
     