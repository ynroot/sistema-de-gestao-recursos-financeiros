<?php
App::uses('AppController', 'Controller');
/**
 * Recibos Controller
 *
 * @property Recibo $Recibo
 * @property PaginatorComponent $Paginator
 */
class RecibosController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Recibo->recursive = 0;
		$this->set('recibos', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Recibo->exists($id)) {
			throw new NotFoundException(__('Invalid recibo'));
		}
		$options = array('conditions' => array('Recibo.' . $this->Recibo->primaryKey => $id));
		$this->set('recibo', $this->Recibo->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Recibo->create();
			if ($this->Recibo->save($this->request->data)) {
				$this->Session->setFlash(__('The recibo has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The recibo could not be saved. Please, try again.'), 'flash/error');
			}
		}
		$bills = $this->Recibo->Bill->find('list');
		$this->set(compact('bills'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
        $this->Recibo->id = $id;
		if (!$this->Recibo->exists($id)) {
			throw new NotFoundException(__('Invalid recibo'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Recibo->save($this->request->data)) {
				$this->Session->setFlash(__('The recibo has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The recibo could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Recibo.' . $this->Recibo->primaryKey => $id));
			$this->request->data = $this->Recibo->find('first', $options);
		}
		$bills = $this->Recibo->Bill->find('list');
		$this->set(compact('bills'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Recibo->id = $id;
		if (!$this->Recibo->exists()) {
			throw new NotFoundException(__('Invalid recibo'));
		}
		if ($this->Recibo->delete()) {
			$this->Session->setFlash(__('Recibo deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Recibo was not deleted'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}
}
