<?php
App::uses('AppController', 'Controller');
/**
 * Accounts Controller
 *
 * @property Account $Account
 * @property PaginatorComponent $Paginator
 */
class AccountsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
//	public function index() {
//		$this->Account->recursive = 0;
//		$this->set('accounts', $this->paginate());
//	}

    public function index($id = null) {
       // $this->loadModel('Account');
        if (!$this->Account->exists($id)) {
            throw new NotFoundException(__('Invalid restaurant'));
        }

        $this->paginate = array(
            'conditions' => array('Account.client_id' => $id)
        );

        $this->Account->recursive = 0;
        $this->set('accounts', $this->Paginator->paginate());

        /**
         * igual a view method
         *
         * @throws NotFoundException
         * @param string $id
         * @return restaurant name
         */
        //$options = array('conditions' => array('Account.'.$this->Task->primaryKey => $id), 'limit'=>1);
        //$this->set('task1', $this->Restaurant->find('first', $options));

    }
	public function conta() {
		$this->paginate = array(
			'conditions' => array('Account.user_id' => $this->Session->read('Auth.User.id'))
		);
        $this->Account->recursive = 0;
        $this->set('accounts', $this->Paginator->paginate());
    }

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Account->exists($id)) {
			throw new NotFoundException(__('Invalid account'));
		}
		$options = array('conditions' => array('Account.' . $this->Account->primaryKey => $id));
		$this->set('account', $this->Account->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Account->create();
			if ($this->Account->save($this->request->data)) {
				$this->Session->setFlash(__('The account has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The account could not be saved. Please, try again.'), 'flash/error');
			}
		}
		$users = $this->Account->User->find('list');
		$this->set(compact('users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
        $this->Account->id = $id;
		if (!$this->Account->exists($id)) {
			throw new NotFoundException(__('Invalid account'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Account->save($this->request->data)) {
				$this->Session->setFlash(__('The account has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The account could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Account.' . $this->Account->primaryKey => $id));
			$this->request->data = $this->Account->find('first', $options);
		}
		$users = $this->Account->User->find('list');
		$this->set(compact('users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Account->id = $id;
		if (!$this->Account->exists()) {
			throw new NotFoundException(__('Invalid account'));
		}
		if ($this->Account->delete()) {
			$this->Session->setFlash(__('Account deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Account was not deleted'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}
    public function account() {


    }

    public function transacao($id1, $id) {

        $this->loadModel('Transaction');

        $conditions = array(
            'Transaction.transactions_type_id' => $id1,
            'Transaction.account_id' => $id);
        $transaction = $this->Transaction->find('all', array('conditions'=> $conditions));

        if ($this->request->is('requested')) {
            return $transaction;
        }
        else {

            $this->set('transactions', $transaction);
        }
    }


    public function userAccounts() {
        $userId = $this->Session->read('Auth.User.id');
        $conditions = array(
            'Account.user_id' => $userId);
        $account = $this->Account->find('all', array('conditions'=> $conditions,'order' => 'Account.id DESC'));

        if ($this->request->is('requested')) {
            return $account;
        }
        else {
            $this->set('accounts', $account);
        }
    }

    public function recursosin() {
        $this->set('title_for_layout', __('Entrada de recursos'));

    }

    public function recursosout() {
        $this->set('title_for_layout', __('Saida de recursos'));
    }



}
