<?php

     App::uses('AppController', 'Controller');

     /**
      * Users Controller
      *
      * @property User $User
      * @property PaginatorComponent $Paginator
      */
     class UsersController extends AppController {

         public $theme = 'CakeAdminLTE';
         public $name = 'Users';
         public $uses = array('User');

         /**
          * Components
          *
          * @var array
          */
         public $components = array('Paginator');

         /**
          * index method
          *
          * @return void
          */
         public function index() {
             $this->User->recursive = 0;
             $this->set('users', $this->paginate());

$userId = $this->Session->read('Auth.User.id');
             $this->User->recursive = 0;
             $this->set('users', $this->paginate(
                                 'User', array(
                                         'User.id'=>$userId
                                 )
             ));

         }

         /**
          * view method
          *
          * @throws NotFoundException
          * @param string $id
          * @return void
          */
         public function view($id = null) {
             if (!$this->User->exists($id)) {
                 throw new NotFoundException(__('Invalid user'));
             }
             $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
             $this->set('user', $this->User->find('first', $options));
         }

         /**
          * add method
          *
          * @return void
          */
         public function add() {
             $this->response->disableCache();
             if ($this->request->is('post')) {
                 $this->User->create();
                 if ($this->User->save($this->request->data)) {
                     $this->Session->setFlash(__('The user has been saved'), 'flash/success');
                     $this->redirect(array('action' => 'index'));
                 } else {
                     $this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'flash/error');
                 }
             }
             $roles = $this->User->Role->find('list');
             $this->set(compact('roles'));
         }

         /**
          * edit method
          *
          * @throws NotFoundException
          * @param string $id
          * @return void
          */
         public function edit($id = null) {
             $this->User->id = $id;
             if (!$this->User->exists($id)) {
                 throw new NotFoundException(__('Invalid user'));
             }
             if ($this->request->is('post') || $this->request->is('put')) {
                 if ($this->User->save($this->request->data)) {
                     $this->Session->setFlash(__('The user has been saved'), 'flash/success');
                     $this->redirect(array('action' => 'index'));
                 } else {
                     $this->Session->setFlash(__('The user could not be saved. Please, try again.'), 'flash/error');
                 }
             } else {
                 $options = array('conditions' => array('User.' . $this->User->primaryKey => $id));
                 $this->request->data = $this->User->find('first', $options);
             }
             $roles = $this->User->Role->find('list');
             $this->set(compact('roles'));
         }

         /**
          * delete method
          *
          * @throws NotFoundException
          * @throws MethodNotAllowedException
          * @param string $id
          * @return void
          */
         public function delete($id = null) {
             if (!$this->request->is('post')) {
                 throw new MethodNotAllowedException();
             }
             $this->User->id = $id;
             if (!$this->User->exists()) {
                 throw new NotFoundException(__('Invalid user'));
             }
             if ($this->User->delete()) {
                 $this->Session->setFlash(__('User deleted'), 'flash/success');
                 $this->redirect(array('action' => 'index'));
             }
             $this->Session->setFlash(__('User was not deleted'), 'flash/error');
             $this->redirect(array('action' => 'index'));
         }

//         public function beforeFilter() {
//             parent::beforeFilter();
//         }

         public function home() {
             $this->set('title_for_layout', __('Painel de control'));
         }

         public function login() {
             $this->theme = false;
             $this->layout='default';
             $this->set('title_for_layout', __('Log in'));

             if ($this->request->is('post')) {
                 if ($this->Auth->login()) {
                     // $this->Session->setFlash(__('login com sucesso.'), 'default', array('class' => 'success'));
                     return $this->redirect(array('controller'=>'users', 'action'=>'home'));
                 } else {
                     $this->Session->setFlash($this->Auth->authError, 'default', array(), 'auth');
                      $this->Session->setFlash(__('Username or password is incorrect'), 'default', array('class' => 'warning'));
                     $this->redirect(array('controller'=>'users', 'action'=>'home'));
                 }
             }
         }

         public function logout() {
             $this->Session->destroy();

//             $this->Session->setFlash(__('Deslogado com sucesso.'), 'default', array('class' => 'success'));
             $this->redirect($this->Auth->logout());
             $this->response->disableCache();
         }

         public function admin_logout() {
             $this->Session->setFlash(__('Deslogado com sucesso.'), 'default', array('class' => 'success'));
             $this->redirect($this->Auth->logout());
         }

         public function calendar(){

         }






     }
