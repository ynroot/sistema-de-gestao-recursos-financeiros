<?php
App::uses('AppController', 'Controller');
/**
 * TransactionsTypes Controller
 *
 * @property TransactionsType $TransactionsType
 * @property PaginatorComponent $Paginator
 */
class TransactionsTypesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->TransactionsType->recursive = 0;
		$this->set('transactionsTypes', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->TransactionsType->exists($id)) {
			throw new NotFoundException(__('Invalid transactions type'));
		}
		$options = array('conditions' => array('TransactionsType.' . $this->TransactionsType->primaryKey => $id));
		$this->set('transactionsType', $this->TransactionsType->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->TransactionsType->create();
			if ($this->TransactionsType->save($this->request->data)) {
				$this->Session->setFlash(__('The transactions type has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The transactions type could not be saved. Please, try again.'), 'flash/error');
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
        $this->TransactionsType->id = $id;
		if (!$this->TransactionsType->exists($id)) {
			throw new NotFoundException(__('Invalid transactions type'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->TransactionsType->save($this->request->data)) {
				$this->Session->setFlash(__('The transactions type has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The transactions type could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('TransactionsType.' . $this->TransactionsType->primaryKey => $id));
			$this->request->data = $this->TransactionsType->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->TransactionsType->id = $id;
		if (!$this->TransactionsType->exists()) {
			throw new NotFoundException(__('Invalid transactions type'));
		}
		if ($this->TransactionsType->delete()) {
			$this->Session->setFlash(__('Transactions type deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Transactions type was not deleted'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}
}
