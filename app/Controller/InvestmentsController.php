<?php
App::uses('AppController', 'Controller');
/**
 * Investments Controller
 *
 * @property Investment $Investment
 * @property PaginatorComponent $Paginator
 */
class InvestmentsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Investment->recursive = 0;
		$this->set('investments', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Investment->exists($id)) {
			throw new NotFoundException(__('Invalid investment'));
		}
		$options = array('conditions' => array('Investment.' . $this->Investment->primaryKey => $id));
		$this->set('investment', $this->Investment->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Investment->create();
			if ($this->Investment->save($this->request->data)) {
				$this->Session->setFlash(__('The investment has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The investment could not be saved. Please, try again.'), 'flash/error');
			}
		}
		$accounts = $this->Investment->Account->find('list');
		$this->set(compact('accounts'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
        $this->Investment->id = $id;
		if (!$this->Investment->exists($id)) {
			throw new NotFoundException(__('Invalid investment'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Investment->save($this->request->data)) {
				$this->Session->setFlash(__('The investment has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The investment could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Investment.' . $this->Investment->primaryKey => $id));
			$this->request->data = $this->Investment->find('first', $options);
		}
		$accounts = $this->Investment->Account->find('list');
		$this->set(compact('accounts'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Investment->id = $id;
		if (!$this->Investment->exists()) {
			throw new NotFoundException(__('Invalid investment'));
		}
		if ($this->Investment->delete()) {
			$this->Session->setFlash(__('Investment deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Investment was not deleted'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}
}
