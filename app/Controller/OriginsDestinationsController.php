<?php
App::uses('AppController', 'Controller');
/**
 * OriginsDestinations Controller
 *
 * @property OriginsDestination $OriginsDestination
 * @property PaginatorComponent $Paginator
 */
class OriginsDestinationsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->OriginsDestination->recursive = 0;
		$this->set('originsDestinations', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->OriginsDestination->exists($id)) {
			throw new NotFoundException(__('Invalid origins destination'));
		}
		$options = array('conditions' => array('OriginsDestination.' . $this->OriginsDestination->primaryKey => $id));
		$this->set('originsDestination', $this->OriginsDestination->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->OriginsDestination->create();
			if ($this->OriginsDestination->save($this->request->data)) {
				$this->Session->setFlash(__('The origins destination has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The origins destination could not be saved. Please, try again.'), 'flash/error');
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
        $this->OriginsDestination->id = $id;
		if (!$this->OriginsDestination->exists($id)) {
			throw new NotFoundException(__('Invalid origins destination'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->OriginsDestination->save($this->request->data)) {
				$this->Session->setFlash(__('The origins destination has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The origins destination could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('OriginsDestination.' . $this->OriginsDestination->primaryKey => $id));
			$this->request->data = $this->OriginsDestination->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->OriginsDestination->id = $id;
		if (!$this->OriginsDestination->exists()) {
			throw new NotFoundException(__('Invalid origins destination'));
		}
		if ($this->OriginsDestination->delete()) {
			$this->Session->setFlash(__('Origins destination deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Origins destination was not deleted'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}
}
