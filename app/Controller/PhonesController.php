<?php
App::uses('AppController', 'Controller');
/**
 * Phones Controller
 *
 * @property Phone $Phone
 * @property PaginatorComponent $Paginator
 */
class PhonesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Phone->recursive = 0;
		$this->set('phones', $this->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Phone->exists($id)) {
			throw new NotFoundException(__('Invalid phone'));
		}
		$options = array('conditions' => array('Phone.' . $this->Phone->primaryKey => $id));
		$this->set('phone', $this->Phone->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Phone->create();
			if ($this->Phone->save($this->request->data)) {
				$this->Session->setFlash(__('The phone has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The phone could not be saved. Please, try again.'), 'flash/error');
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
        $this->Phone->id = $id;
		if (!$this->Phone->exists($id)) {
			throw new NotFoundException(__('Invalid phone'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->Phone->save($this->request->data)) {
				$this->Session->setFlash(__('The phone has been saved'), 'flash/success');
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The phone could not be saved. Please, try again.'), 'flash/error');
			}
		} else {
			$options = array('conditions' => array('Phone.' . $this->Phone->primaryKey => $id));
			$this->request->data = $this->Phone->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @throws MethodNotAllowedException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->Phone->id = $id;
		if (!$this->Phone->exists()) {
			throw new NotFoundException(__('Invalid phone'));
		}
		if ($this->Phone->delete()) {
			$this->Session->setFlash(__('Phone deleted'), 'flash/success');
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Phone was not deleted'), 'flash/error');
		$this->redirect(array('action' => 'index'));
	}
}
