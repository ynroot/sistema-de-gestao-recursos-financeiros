<?php
/**
 * AccountFixture
 *
 */
class AccountFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'title' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 45, 'key' => 'unique', 'collate' => 'utf8_general_ci', 'charset' => 'utf8'),
		'initial_value' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'saldo' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false),
		'dif_transacao' => array('type' => 'float', 'null' => true, 'default' => null, 'unsigned' => false, 'comment' => 'DIFERENCA DAS TRASACOES
'),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'index'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1),
			'title_UNIQUE' => array('column' => 'title', 'unique' => 1),
			'fk_conta_users1_idx' => array('column' => 'user_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'title' => 'Lorem ipsum dolor sit amet',
			'initial_value' => 1,
			'saldo' => 1,
			'dif_transacao' => 1,
			'user_id' => 1,
			'created' => '2014-11-06 18:55:02',
			'modified' => '2014-11-06 18:55:02'
		),
	);

}
