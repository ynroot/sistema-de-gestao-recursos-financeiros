<?php
/**
 * AccountsHasBudgetFixture
 *
 */
class AccountsHasBudgetFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'account_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'budget_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'indexes' => array(
			'PRIMARY' => array('column' => array('account_id', 'budget_id'), 'unique' => 1),
			'fk_accounts_has_budgets_budgets1_idx' => array('column' => 'budget_id', 'unique' => 0),
			'fk_accounts_has_budgets_accounts1_idx' => array('column' => 'account_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'account_id' => 1,
			'budget_id' => 1
		),
	);

}
