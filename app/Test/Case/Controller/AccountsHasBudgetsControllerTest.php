<?php
App::uses('AccountsHasBudgetsController', 'Controller');

/**
 * AccountsHasBudgetsController Test Case
 *
 */
class AccountsHasBudgetsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.accounts_has_budget',
		'app.account',
		'app.user',
		'app.role',
		'app.investment',
		'app.patrimonio',
		'app.transaction',
		'app.transactions_type',
		'app.categories_type',
		'app.origins_destination',
		'app.budget',
		'app.bill',
		'app.payment'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
		$this->markTestIncomplete('testIndex not implemented.');
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
		$this->markTestIncomplete('testView not implemented.');
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
		$this->markTestIncomplete('testAdd not implemented.');
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
		$this->markTestIncomplete('testEdit not implemented.');
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
		$this->markTestIncomplete('testDelete not implemented.');
	}

}
