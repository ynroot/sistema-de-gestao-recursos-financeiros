<?php
App::uses('Fool', 'Model');

/**
 * Fool Test Case
 *
 */
class FoolTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.fool'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Fool = ClassRegistry::init('Fool');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Fool);

		parent::tearDown();
	}

}
