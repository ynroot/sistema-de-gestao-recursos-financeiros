<?php
App::uses('AccountsHasBudget', 'Model');

/**
 * AccountsHasBudget Test Case
 *
 */
class AccountsHasBudgetTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.accounts_has_budget',
		'app.account',
		'app.user',
		'app.role',
		'app.investment',
		'app.patrimonio',
		'app.transaction',
		'app.transactions_type',
		'app.categories_type',
		'app.origins_destination',
		'app.budget',
		'app.bill',
		'app.payment'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->AccountsHasBudget = ClassRegistry::init('AccountsHasBudget');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->AccountsHasBudget);

		parent::tearDown();
	}

}
