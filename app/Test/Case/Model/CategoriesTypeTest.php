<?php
App::uses('CategoriesType', 'Model');

/**
 * CategoriesType Test Case
 *
 */
class CategoriesTypeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.categories_type',
		'app.transaction',
		'app.transactions_type',
		'app.account',
		'app.origins_destination'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->CategoriesType = ClassRegistry::init('CategoriesType');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->CategoriesType);

		parent::tearDown();
	}

}
