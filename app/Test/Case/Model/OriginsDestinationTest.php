<?php
App::uses('OriginsDestination', 'Model');

/**
 * OriginsDestination Test Case
 *
 */
class OriginsDestinationTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.origins_destination',
		'app.transaction',
		'app.transactions_type',
		'app.account',
		'app.categories_type'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->OriginsDestination = ClassRegistry::init('OriginsDestination');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->OriginsDestination);

		parent::tearDown();
	}

}
