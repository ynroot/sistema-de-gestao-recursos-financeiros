<?php
App::uses('TransactionsType', 'Model');

/**
 * TransactionsType Test Case
 *
 */
class TransactionsTypeTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.transactions_type',
		'app.transaction',
		'app.account',
		'app.categories_type',
		'app.origins_destination'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->TransactionsType = ClassRegistry::init('TransactionsType');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->TransactionsType);

		parent::tearDown();
	}

}
