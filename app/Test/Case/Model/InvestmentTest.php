<?php
App::uses('Investment', 'Model');

/**
 * Investment Test Case
 *
 */
class InvestmentTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.investment',
		'app.account',
		'app.user',
		'app.role',
		'app.patrimonio',
		'app.transaction',
		'app.transactions_type',
		'app.categories_type',
		'app.origins_destination'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Investment = ClassRegistry::init('Investment');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Investment);

		parent::tearDown();
	}

}
