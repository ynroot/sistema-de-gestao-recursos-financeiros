
<div class="row">
    <div class="col-xs-12">
		
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title"><?php  echo __('Payment'); ?></h3>
				<div class="box-tools pull-right">
	                <?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i> Edit'), array('action' => 'edit', $payment['Payment']['id']), array('class' => 'btn btn-primary', 'escape' => false)); ?>
	            </div>
			</div>
			
			<div class="box-body table-responsive">
                <table id="Payments" class="table table-bordered table-striped">
					<tbody>
						<tr>		<td><strong><?php echo __('Id'); ?></strong></td>
		<td>
			<?php echo h($payment['Payment']['id']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Name'); ?></strong></td>
		<td>
			<?php echo h($payment['Payment']['name']); ?>
			&nbsp;
		</td>
</tr>					</tbody>
				</table><!-- /.table table-striped table-bordered -->
			</div><!-- /.table-responsive -->
			
		</div><!-- /.view -->

					
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title"><?php echo __('Related Bills'); ?></h3>
					<div class="box-tools pull-right">
						<?php echo $this->Html->link('<i class="glyphicon glyphicon-plus"></i> '.__('New Bill'), array('controller' => 'bills', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>					</div><!-- /.actions -->
				</div>
				<?php if (!empty($payment['Bill'])): ?>
					
					<div class="box-body table-responsive">
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
											<th class="text-center"><?php echo __('Id'); ?></th>
		<th class="text-center"><?php echo __('Item'); ?></th>
		<th class="text-center"><?php echo __('Um'); ?></th>
		<th class="text-center"><?php echo __('Quantidade'); ?></th>
		<th class="text-center"><?php echo __('Descrição'); ?></th>
		<th class="text-center"><?php echo __('Preço'); ?></th>
		<th class="text-center"><?php echo __('Budget Id'); ?></th>
		<th class="text-center"><?php echo __('Payment Id'); ?></th>
									<th class="text-center"><?php echo __('Actions'); ?></th>
								</tr>
							</thead>
							<tbody>
									<?php
										$i = 0;
										foreach ($payment['Bill'] as $bill): ?>
		<tr>
			<td class="text-center"><?php echo $bill['id']; ?></td>
			<td class="text-center"><?php echo $bill['item']; ?></td>
			<td class="text-center"><?php echo $bill['um']; ?></td>
			<td class="text-center"><?php echo $bill['quantidade']; ?></td>
			<td class="text-center"><?php echo $bill['descrição']; ?></td>
			<td class="text-center"><?php echo $bill['preço']; ?></td>
			<td class="text-center"><?php echo $bill['budget_id']; ?></td>
			<td class="text-center"><?php echo $bill['payment_id']; ?></td>
			<td class="text-center">
				<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-eye-open"></i>'), array('controller' => 'bills', 'action' => 'view', $bill['id']), array('class' => 'btn btn-primary btn-xs', 'escape' => false)); ?>
				<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i>'), array('controller' => 'bills', 'action' => 'edit', $bill['id']), array('class' => 'btn btn-warning btn-xs', 'escape' => false)); ?>
				<?php echo $this->Form->postLink(__('<i class="glyphicon glyphicon-trash"></i>'), array('controller' => 'bills', 'action' => 'delete', $bill['id']), array('class' => 'btn btn-danger btn-xs', 'escape' => false), __('Are you sure you want to delete # %s?', $bill['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
							</tbody>
						</table><!-- /.table table-striped table-bordered -->
					</div><!-- /.table-responsive -->
					
				<?php endif; ?>

				
				
			</div><!-- /.related -->

			
	</div><!-- /#page-content .span9 -->

</div><!-- /#page-container .row-fluid -->
