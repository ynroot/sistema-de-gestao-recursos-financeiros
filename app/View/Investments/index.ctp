<?php echo $this->Html->css('datatables/dataTables.bootstrap'); ?>
<div class="row">
    <div class="col-xs-12">

    <div class="box box-primary">
		<div class="box-header">
			<h3 class="box-title"><?php echo __('Investments'); ?></h3>
			<div class="box-tools pull-right">
                <?php echo $this->Html->link(__('<i class="glyphicon glyphicon-plus"></i> New Investment'), array('action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>
            </div>
		</div>	
			<div class="box-body table-responsive">
                <table id="Investments" class="table table-bordered table-striped">
					<thead>
						<tr>
													<th class="text-center"><?php echo $this->Paginator->sort('id'); ?></th>
													<th class="text-center"><?php echo $this->Paginator->sort('name'); ?></th>
													<th class="text-center"><?php echo $this->Paginator->sort('account_id'); ?></th>
													<th class="text-center"><?php echo $this->Paginator->sort('duration'); ?></th>
													<th class="text-center"><?php echo $this->Paginator->sort('juros'); ?></th>
													<th class="text-center"><?php echo $this->Paginator->sort('created'); ?></th>
													<th class="text-center"><?php echo $this->Paginator->sort('modified'); ?></th>
												<th class="text-center"><?php echo __('Actions'); ?></th>
						</tr>
					</thead>
					<tbody>
					<?php foreach ($investments as $investment): ?>
	<tr>
		<td class="text-center"><?php echo h($investment['Investment']['id']); ?>&nbsp;</td>
		<td class="text-center"><?php echo h($investment['Investment']['name']); ?>&nbsp;</td>
		<td class="text-center">
			<?php echo $this->Html->link($investment['Account']['title'], array('controller' => 'accounts', 'action' => 'view', $investment['Account']['id'])); ?>
		</td>
		<td class="text-center"><?php echo h($investment['Investment']['duration']); ?>&nbsp;</td>
		<td class="text-center"><?php echo h($investment['Investment']['juros']); ?>&nbsp;</td>
		<td class="text-center"><?php echo h($investment['Investment']['created']); ?>&nbsp;</td>
		<td class="text-center"><?php echo h($investment['Investment']['modified']); ?>&nbsp;</td>
		<td class="text-center">
			<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-eye-open"></i>'), array('action' => 'view', $investment['Investment']['id']), array('class' => 'btn btn-primary btn-xs', 'escape' => false)); ?>
			<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i>'), array('action' => 'edit', $investment['Investment']['id']), array('class' => 'btn btn-warning btn-xs', 'escape' => false)); ?>
			<?php echo $this->Form->postLink(__('<i class="glyphicon glyphicon-trash"></i>'), array('action' => 'delete', $investment['Investment']['id']), array('class' => 'btn btn-danger btn-xs', 'escape' => false), __('Are you sure you want to delete # %s?', $investment['Investment']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
					</tbody>
				</table>
			</div><!-- /.table-responsive -->
			
			
		</div><!-- /.index -->
	
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->

<?php
	echo $this->Html->script('jquery.min');
	echo $this->Html->script('plugins/datatables/jquery.dataTables');
	echo $this->Html->script('plugins/datatables/dataTables.bootstrap');
?>
<script type="text/javascript">
    $(function() {
        $("#Investments").dataTable();
    });
</script>