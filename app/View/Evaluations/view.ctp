
<div class="row">
    <div class="col-xs-12">
		
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title"><?php  echo __('Evaluation'); ?></h3>
				<div class="box-tools pull-right">
	                <?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i> Edit'), array('action' => 'edit', $evaluation['Evaluation']['id']), array('class' => 'btn btn-primary', 'escape' => false)); ?>
	            </div>
			</div>
			
			<div class="box-body table-responsive">
                <table id="Evaluations" class="table table-bordered table-striped">
					<tbody>
						<tr>		<td><strong><?php echo __('Id'); ?></strong></td>
		<td>
			<?php echo h($evaluation['Evaluation']['id']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Title'); ?></strong></td>
		<td>
			<?php echo h($evaluation['Evaluation']['title']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Created'); ?></strong></td>
		<td>
			<?php echo h($evaluation['Evaluation']['created']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Modified'); ?></strong></td>
		<td>
			<?php echo h($evaluation['Evaluation']['modified']); ?>
			&nbsp;
		</td>
</tr>					</tbody>
				</table><!-- /.table table-striped table-bordered -->
			</div><!-- /.table-responsive -->
			
		</div><!-- /.view -->

					
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title"><?php echo __('Related Patrimonios'); ?></h3>
					<div class="box-tools pull-right">
						<?php echo $this->Html->link('<i class="glyphicon glyphicon-plus"></i> '.__('New Patrimonio'), array('controller' => 'patrimonios', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>					</div><!-- /.actions -->
				</div>
				<?php if (!empty($evaluation['Patrimonio'])): ?>
					
					<div class="box-body table-responsive">
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
											<th class="text-center"><?php echo __('Id'); ?></th>
		<th class="text-center"><?php echo __('Name'); ?></th>
		<th class="text-center"><?php echo __('Time Evaluation'); ?></th>
		<th class="text-center"><?php echo __('Valor '); ?></th>
		<th class="text-center"><?php echo __('Taxa Percentagem'); ?></th>
		<th class="text-center"><?php echo __('Obs'); ?></th>
		<th class="text-center"><?php echo __('Account Id'); ?></th>
		<th class="text-center"><?php echo __('Created'); ?></th>
		<th class="text-center"><?php echo __('Modified'); ?></th>
		<th class="text-center"><?php echo __('Evaluation Id'); ?></th>
									<th class="text-center"><?php echo __('Actions'); ?></th>
								</tr>
							</thead>
							<tbody>
									<?php
										$i = 0;
										foreach ($evaluation['Patrimonio'] as $patrimonio): ?>
		<tr>
			<td class="text-center"><?php echo $patrimonio['id']; ?></td>
			<td class="text-center"><?php echo $patrimonio['name']; ?></td>
			<td class="text-center"><?php echo $patrimonio['time_evaluation']; ?></td>
			<td class="text-center"><?php echo $patrimonio['valor_']; ?></td>
			<td class="text-center"><?php echo $patrimonio['taxa_percentagem']; ?></td>
			<td class="text-center"><?php echo $patrimonio['obs']; ?></td>
			<td class="text-center"><?php echo $patrimonio['account_id']; ?></td>
			<td class="text-center"><?php echo $patrimonio['created']; ?></td>
			<td class="text-center"><?php echo $patrimonio['modified']; ?></td>
			<td class="text-center"><?php echo $patrimonio['evaluation_id']; ?></td>
			<td class="text-center">
				<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-eye-open"></i>'), array('controller' => 'patrimonios', 'action' => 'view', $patrimonio['id']), array('class' => 'btn btn-primary btn-xs', 'escape' => false)); ?>
				<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i>'), array('controller' => 'patrimonios', 'action' => 'edit', $patrimonio['id']), array('class' => 'btn btn-warning btn-xs', 'escape' => false)); ?>
				<?php echo $this->Form->postLink(__('<i class="glyphicon glyphicon-trash"></i>'), array('controller' => 'patrimonios', 'action' => 'delete', $patrimonio['id']), array('class' => 'btn btn-danger btn-xs', 'escape' => false), __('Are you sure you want to delete # %s?', $patrimonio['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
							</tbody>
						</table><!-- /.table table-striped table-bordered -->
					</div><!-- /.table-responsive -->
					
				<?php endif; ?>

				
				
			</div><!-- /.related -->

			
	</div><!-- /#page-content .span9 -->

</div><!-- /#page-container .row-fluid -->
