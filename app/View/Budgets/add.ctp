<style>
	.t1{
		width:100px;
	}
	.t2{
		width:90px;
	}
</style>
<div class="row">
    <div class="col-xs-11">
		<div class="box box-primary">
			<div class="box-header">
			<h3 class="box-title"><?php echo __('Add Budget'); ?></h3>
			</div>
			<div class="box-body table-responsive">
		
			<?php echo $this->Form->create('Budget', array('role' => 'form','class'=>'form-inline')); ?>

				<fieldset>

										<div class="form-group">
						<?php echo $this->Form->input('item', array('class' => 'form-control t1')); ?>
					</div><!-- .form-group -->
					<div class="form-group">
						<?php echo $this->Form->input('um', array('class' => 'form-control  t2')); ?>
					</div><!-- .form-group -->
					<div class="form-group">
						<?php echo $this->Form->input('quantidade', array('class' => 'form-control t2')); ?>
					</div><!-- .form-group -->
<!--					<div class="form-group">-->
<!--						--><?php //echo $this->Form->input('descrição', array('class' => 'form-control t1')); ?>
<!--					</div><!-- .form-group -->
					<div class="form-group">
						<?php echo $this->Form->input('preço', array('class' => 'form-control t2')); ?>
					</div><!-- .form-group -->
					<div class="form-group">
						<small> </small><a href=""><i class="fa fa-plus-circle fa-2x"></i></a>
					</div><!-- .form-group -->
					<p>-</p>

					<?php echo $this->Form->submit('Submit', array('class' => 'btn btn-large btn-primary')); ?>

				</fieldset>

						<?php echo $this->Form->end(); ?>

		</div><!-- /.form -->
			
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->