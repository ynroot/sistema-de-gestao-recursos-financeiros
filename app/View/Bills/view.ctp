
<div class="row">
    <div class="col-xs-12">
		
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title"><?php  echo __('Bill'); ?></h3>
				<div class="box-tools pull-right">
	                <?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i> Edit'), array('action' => 'edit', $bill['Bill']['id']), array('class' => 'btn btn-primary', 'escape' => false)); ?>
	            </div>
			</div>
			
			<div class="box-body table-responsive">
                <table id="Bills" class="table table-bordered table-striped">
					<tbody>
						<tr>		<td><strong><?php echo __('Id'); ?></strong></td>
		<td>
			<?php echo h($bill['Bill']['id']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Item'); ?></strong></td>
		<td>
			<?php echo h($bill['Bill']['item']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Um'); ?></strong></td>
		<td>
			<?php echo h($bill['Bill']['um']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Quantidade'); ?></strong></td>
		<td>
			<?php echo h($bill['Bill']['quantidade']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Descrição'); ?></strong></td>
		<td>
			<?php echo h($bill['Bill']['descrição']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Preço'); ?></strong></td>
		<td>
			<?php echo h($bill['Bill']['preço']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Budget'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($bill['Budget']['id'], array('controller' => 'budgets', 'action' => 'view', $bill['Budget']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Payment'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($bill['Payment']['name'], array('controller' => 'payments', 'action' => 'view', $bill['Payment']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr>					</tbody>
				</table><!-- /.table table-striped table-bordered -->
			</div><!-- /.table-responsive -->
			
		</div><!-- /.view -->

			
	</div><!-- /#page-content .span9 -->

</div><!-- /#page-container .row-fluid -->
