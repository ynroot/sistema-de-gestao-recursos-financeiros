
<div class="row">
    <div class="col-xs-12">

        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"><?php echo __('Transaction'); ?></h3>
                <div class="box-tools pull-right">
                    <?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i> Edit'), array('action' => 'edit', $transaction['Transaction']['id']), array('class' => 'btn btn-primary', 'escape' => false)); ?>
                </div>
            </div>

            <div class="box-body table-responsive">
                <table id="Transactions" class="table table-bordered table-striped">
                    <tbody>
                        <tr>		<td><strong><?php echo __('Id'); ?></strong></td>
                            <td>
                                <?php echo h($transaction['Transaction']['id']); ?>
                                &nbsp;
                            </td>
                        </tr><tr>		<td><strong><?php echo __('User'); ?></strong></td>
                            <td>
                                <?php echo $this->Html->link($transaction['User']['id'], array('controller' => 'users', 'action' => 'view', $transaction['User']['id']), array('class' => '')); ?>
                                &nbsp;
                            </td>
                        </tr><tr>		<td><strong><?php echo __('Transactions Type'); ?></strong></td>
                            <td>
                                <?php echo $this->Html->link($transaction['TransactionsType']['title'], array('controller' => 'transactions_types', 'action' => 'view', $transaction['TransactionsType']['id']), array('class' => '')); ?>
                                &nbsp;
                            </td>
                        </tr><tr>		<td><strong><?php echo __('Account'); ?></strong></td>
                            <td>
                                <?php echo $this->Html->link($transaction['Account']['title'], array('controller' => 'accounts', 'action' => 'view', $transaction['Account']['id']), array('class' => '')); ?>
                                &nbsp;
                            </td>
                        </tr><tr>		<td><strong><?php echo __('Valor'); ?></strong></td>
                            <td>
                                <?php echo h($transaction['Transaction']['valor']); ?>
                                &nbsp;
                            </td>
                        </tr><tr>		<td><strong><?php echo __('Obs'); ?></strong></td>
                            <td>
                                <?php echo h($transaction['Transaction']['obs']); ?>
                                &nbsp;
                            </td>
                        </tr><tr>		<td><strong><?php echo __('Categories Type'); ?></strong></td>
                            <td>
                                <?php echo $this->Html->link($transaction['CategoriesType']['title'], array('controller' => 'categories_types', 'action' => 'view', $transaction['CategoriesType']['id']), array('class' => '')); ?>
                                &nbsp;
                            </td>
                        </tr><tr>		<td><strong><?php echo __('Origins Destination'); ?></strong></td>
                            <td>
                                <?php echo $this->Html->link($transaction['OriginsDestination']['title'], array('controller' => 'origins_destinations', 'action' => 'view', $transaction['OriginsDestination']['id']), array('class' => '')); ?>
                                &nbsp;
                            </td>
                        </tr><tr>		<td><strong><?php echo __('Created'); ?></strong></td>
                            <td>
                                <?php echo h($transaction['Transaction']['created']); ?>
                                &nbsp;
                            </td>
                        </tr><tr>		<td><strong><?php echo __('Modified'); ?></strong></td>
                            <td>
                                <?php echo h($transaction['Transaction']['modified']); ?>
                                &nbsp;
                            </td>
                        </tr>					</tbody>
                </table><!-- /.table table-striped table-bordered -->
            </div><!-- /.table-responsive -->

        </div><!-- /.view -->


    </div><!-- /#page-content .span9 -->

</div><!-- /#page-container .row-fluid -->
