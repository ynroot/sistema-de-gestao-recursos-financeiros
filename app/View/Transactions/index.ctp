<?php echo $this->Html->css('datatables/dataTables.bootstrap');
?>
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div class="box-header">

                <div class="box-tools pull-right">
                    <?php echo $this->Html->link(__('<i class="glyphicon glyphicon-plus"></i> New Transaction'), array('action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>
                </div>
            </div>
            <div class="box-body table-responsive">
                <table id="Transactions" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th class="text-center"><?php echo $this->Paginator->sort('id'); ?></th>
                            <!--                            <th class="text-center"><?php //echo $this->Paginator->sort('user_id');  ?></th>-->
                            <th class="text-center"><?php echo $this->Paginator->sort('transactions_type_id'); ?></th>
                            <th class="text-center"><?php echo $this->Paginator->sort('account_id'); ?></th>
                            <th class="text-center"><?php echo $this->Paginator->sort('valor'); ?></th>
                            <th class="text-center"><?php echo $this->Paginator->sort('obs'); ?></th>
                            <th class="text-center"><?php echo $this->Paginator->sort('categories_type_id'); ?></th>
                            <th class="text-center"><?php echo $this->Paginator->sort('origins_destination_id'); ?></th>
                            <th class="text-center"><?php echo $this->Paginator->sort('created'); ?></th>
                            <th class="text-center"><?php echo $this->Paginator->sort('modified'); ?></th>
                            <th class="text-center"><?php echo __('Actions'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($transactions as $transaction): ?>
                        <tr>
                            <td class="text-center"><?php echo h($transaction['Transaction']['id']); ?>&nbsp;</td>
                            <!--                                     <td class="text-center">
                                <?php //echo $this->Html->link($transaction['User']['username'], array('controller' => 'users', 'action' => 'view', $transaction['User']['id'])); ?>
                            </td>-->
                            <td class="text-center">
                                <?php echo $this->Html->link($transaction['TransactionsType']['title'], array('controller' => 'transactions_types', 'action' => 'view', $transaction['TransactionsType']['id'])); ?>
                            </td>
                            <td class="text-center">
                                <?php echo $this->Html->link($transaction['Account']['title'], array('controller' => 'accounts', 'action' => 'view', $transaction['Account']['id'])); ?>
                            </td>
                            <td class="text-center"><?php  echo $this->number->format($transaction['Transaction']['valor'],
                                                                                                array(
                                                                                                    'places' => 2,
                                                                                                    'before' => '',
                                                                                                    'after' => '$00',
                                                                                                    'escape' => false,
                                                                                                    'decimals' => '.',
                                                                                                    'thousands' => ','
                                                                                                ));?>
                                                                                                &nbsp;</td>
                            <td class="text-center"><?php echo h($transaction['Transaction']['obs']); ?>&nbsp;</td>
                            <td class="text-center">
                                <?php echo $this->Html->link($transaction['CategoriesType']['title'], array('controller' => 'categories_types', 'action' => 'view', $transaction['CategoriesType']['id'])); ?>
                            </td>
                            <td class="text-center">
                                <?php echo $this->Html->link($transaction['OriginsDestination']['title'], array('controller' => 'origins_destinations', 'action' => 'view', $transaction['OriginsDestination']['id'])); ?>
                            </td>
                            <td class="text-center"><?php echo h($this->Time->nice($transaction['OriginsDestination']['created'])); ?>&nbsp;</td>
                            <td class="text-center"><?php echo h($this->Time->nice($transaction['OriginsDestination']['modified'])); ?>&nbsp;</td>
                            <td class="text-center">
                                <?php echo $this->Html->link(__('<i class="glyphicon glyphicon-eye-open"></i>'), array('action' => 'view', $transaction['Transaction']['id']), array('class' => 'btn btn-primary btn-xs', 'escape' => false)); ?>
                                <?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i>'), array('action' => 'edit', $transaction['Transaction']['id']), array('class' => 'btn btn-warning btn-xs', 'escape' => false)); ?>
                                <?php echo $this->Form->postLink(__('<i class="glyphicon glyphicon-trash"></i>'), array('action' => 'delete', $transaction['Transaction']['id']), array('class' => 'btn btn-danger btn-xs', 'escape' => false), __('Are you sure you want to delete # %s?', $transaction['Transaction']['id'])); ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                </div><!-- /.table-responsive -->
                </div><!-- /.index -->
                </div><!-- /#page-content .col-sm-9 -->
                </div><!-- /#page-container .row-fluid -->
                <?php
                    echo $this->Html->script('jquery.min');
                    echo $this->Html->script('plugins/datatables/jquery.dataTables');
                    echo $this->Html->script('plugins/datatables/dataTables.bootstrap');
                ?>
                <script type="text/javascript">
                    $(function () {
                        $("#Transactions").dataTable();
                    });
                </script>