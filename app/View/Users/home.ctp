<?php  $this->set('title_for_layout' , __('Home ') ); ?>
<script>
 window.onload = function(){
        var ctxbar1 = document.getElementById("canvas1").getContext("2d");
        window.myBar = new Chart(ctxbar1).Bar(barChartData, {
            responsive : true
        });
        
        var ctx = document.getElementById("canvas").getContext("2d");
                    window.myLine = new Chart(ctx).Line(lineChartData, {
                        responsive: true
                    });
    }
</script>

<div class="row">
<!-- pagamento -->
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-red">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="glyphicon glyphicon-arrow-up fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">13</div>
                        <div><?php echo __('payments') ?></div>
                    </div>
                </div>
            </div>
            <a href="#">
            <div class="panel-footer">
                <span class="pull-left">View Details</span>
                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                <div class="clearfix"></div>
            </div>
            </a>
        </div>
    </div>
    <!-- deposito -->
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-green">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="glyphicon glyphicon-arrow-down fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">12</div>
                        <div><?php echo __('deposits') ?></div>
                    </div>
                </div>
            </div>
            <a href="#">
            <div class="panel-footer">
                <span class="pull-left">View Details</span>
                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                <div class="clearfix"></div>
            </div>
            </a>
        </div>
    </div>
    
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-credit-card fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">26</div>
                        <div><?php echo __('investments') ?></div>
                    </div>
                </div>
            </div>
            <a href="#">
            <div class="panel-footer">
                <span class="pull-left">View Details</span>
                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                <div class="clearfix"></div>
            </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-yellow">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-inbox fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">124</div>
                        <div><?php echo __('balance available') ?></div>
                    </div>
                </div>
            </div>
            <a href="#">
            <div class="panel-footer">
                <span class="pull-left">View Details</span>
                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                <div class="clearfix"></div>
            </div>
            </a>
        </div>
    </div>

</div>

<!--grafico #1-->

<!-- novo -->
<div class="col-lg-12">             

    <div class="panel panel-default">
        <div class="panel-heading">legenda</div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="table-responsive">
                <div style="width:100%"><!-- canvas -->
                    <div>
                        <canvas id="canvas" height="200" width="600"></canvas>
                    </div>
                </div><!-- canvas -->
            </div>
        </div>
    </div> 


    <script>
		var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
		var lineChartData = {
			labels : ["Janeiro","Fevereiro","Março","Abril","Maio","Juhno","Julho","Agosto","Setembro","Outubro","Novenbro","Dezenbro"],
			datasets : [
				{
					label: "Deposito",
                                fillColor : "rgba(156, 230, 156, 1)",
                                strokeColor : "rgba(55, 186, 55, 1)",
                                pointColor : "rgba(14, 230, 14, 1)",
                                pointStrokeColor : "#fff",
                                pointHighlightFill : "#fff",
                                pointHighlightStroke : "rgba(220,220,220,1)",
					data : [10, 5, 8, 8, 56, 5, 40, 80, 41, 56, 55, 40]
				},
				{
					label: "Pagamento",
                                fillColor : "rgba(129, 89, 89, 0.40)",
                                strokeColor : "rgba(239, 70, 67, 1)",
                                pointColor : "rgba(251, 8, 8, 1)",
                                pointStrokeColor : "#fff",
                                pointHighlightFill : "#fff",
                                pointHighlightStroke : "rgba(151,187,205,1)",
					data : [10, 9, 8, 8, 6, 5, 40, 0, 1, 6, 5, 4]
				}
                                ,
				{
					 label: "Patrimonio",
                                fillColor : "rgba(52, 59, 209, 0.40)",
                                strokeColor : "rgba(151,187,205,1)",
                                pointColor : "rgba(8, 28, 251, 1)",
                                pointStrokeColor : "#fff",
                                pointHighlightFill : "#fff",
                                pointHighlightStroke : "rgba(151,187,205,1)",
					data : [6, 9, 18, 1, 16, 15, 4, 80, 8, 56, 5, 3]
				}
			]

		}

	


	</script>
</div>     
<!-- /novo -->

<div class="col-lg-12">

    <div class="panel panel-default">
        <div class="panel-heading">legenda</div><!-- /.panel-heading -->                          
        <div class="panel-body">
            <div class="table-responsive">

                <div style="width: 100%">
                    <canvas id="canvas1" height="200" width="600"></canvas>
                </div>


               <script>
	var randomScalingFactor = function(){ return Math.round(Math.random()*100)};

	var barChartData = {
		labels : ["Janeiro","Fevereiro","Março","Abril","Maio","Juhno","Julho","Agosto","Setembro","Outubro","Novenbro","Dezenbro"],
		datasets : [
			{
				fillColor : "rgba(55, 186, 55, 1)",
                                        strokeColor : "rgba(55, 186, 55, 1)",
                                        highlightFill: "rgba(55, 186, 55, 1)",
                                        highlightStroke: "rgba(55, 186, 55, 1)",
				data : [6, 9, 18, 1, 16, 15, 4, 80, 8, 56, 5, 3]
			},
			{
				fillColor : "rgba(239, 70, 67, 1)",
                                        strokeColor : "rgba(239, 70, 67, 1)",
                                        highlightFill : "rgba(239, 70, 67, 1)",
                                        highlightStroke : "rgba(239, 70, 67, 1)",
				data : [6, 9, 18, 1, 16, 15, 4, 80, 8, 56, 5, 3]
			},
			{
				fillColor : "rgba(8, 28, 251, 1)",
                                        strokeColor : "rgba(8, 28, 251, 1)",
                                        highlightFill : "rgba(8, 28, 251, 1)",
                                        highlightStroke : "rgba(8, 28, 251, 1)",
				data : [6, 9, 18, 1, 16, 15, 4, 80, 8, 56, 5, 3]
			}
		]

	}
	</script>
            </div>
        </div>
    </div>


</div><!-- 12 -->




