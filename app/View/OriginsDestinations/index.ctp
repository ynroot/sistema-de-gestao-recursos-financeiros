<?php echo $this->Html->css('datatables/dataTables.bootstrap'); ?>
<div class="row">
    <div class="col-xs-12">

    <div class="box box-primary">
		<div class="box-header">

			<div class="box-tools pull-right">
                <?php echo $this->Html->link(__('<i class="glyphicon glyphicon-plus"></i> New Origins Destination'), array('action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>
            </div>
		</div>	
			<div class="box-body table-responsive">
                <table id="OriginsDestinations" class="table table-bordered table-striped">
					<thead>
						<tr>
													<th class="text-center"><?php echo $this->Paginator->sort('id'); ?></th>
													<th class="text-center"><?php echo $this->Paginator->sort('title'); ?></th>
													<th class="text-center"><?php echo $this->Paginator->sort('color'); ?></th>
													<th class="text-center"><?php echo $this->Paginator->sort('created'); ?></th>
													<th class="text-center"><?php echo $this->Paginator->sort('modified'); ?></th>
												<th class="text-center"><?php echo __('Actions'); ?></th>
						</tr>
					</thead>
					<tbody>
					<?php foreach ($originsDestinations as $originsDestination): ?>
	<tr>
		<td class="text-center"><?php echo h($originsDestination['OriginsDestination']['id']); ?>&nbsp;</td>
		<td class="text-center"><?php echo h($originsDestination['OriginsDestination']['title']); ?>&nbsp;</td>
                <td class="text-center"><i class="fa fa-circle fa-1x" style="color:<?php echo h($originsDestination['OriginsDestination']['color']); ?> "></i>&nbsp;</td>
		<td class="text-center"><?php echo h($this->Time->nice($originsDestination['OriginsDestination']['created'])); ?>&nbsp;</td>
		<td class="text-center"><?php echo h($this->Time->nice($originsDestination['OriginsDestination']['created'])); ?>&nbsp;</td>
		<td class="text-center">
			<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-eye-open"></i>'), array('action' => 'view', $originsDestination['OriginsDestination']['id']), array('class' => 'btn btn-primary btn-xs', 'escape' => false)); ?>
			<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i>'), array('action' => 'edit', $originsDestination['OriginsDestination']['id']), array('class' => 'btn btn-warning btn-xs', 'escape' => false)); ?>
			<?php echo $this->Form->postLink(__('<i class="glyphicon glyphicon-trash"></i>'), array('action' => 'delete', $originsDestination['OriginsDestination']['id']), array('class' => 'btn btn-danger btn-xs', 'escape' => false), __('Are you sure you want to delete # %s?', $originsDestination['OriginsDestination']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
					</tbody>
				</table>
			</div><!-- /.table-responsive -->
			
			
		</div><!-- /.index -->
	
	</div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->

<?php
	echo $this->Html->script('jquery.min');
	echo $this->Html->script('plugins/datatables/jquery.dataTables');
	echo $this->Html->script('plugins/datatables/dataTables.bootstrap');
?>
<script type="text/javascript">
    $(function() {
        $("#OriginsDestinations").dataTable();
    });
</script>