
<div class="row">
    <div class="col-xs-12">
		
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title"><?php  echo __('Origins Destination'); ?></h3>
				<div class="box-tools pull-right">
	                <?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i> Edit'), array('action' => 'edit', $originsDestination['OriginsDestination']['id']), array('class' => 'btn btn-primary', 'escape' => false)); ?>
	            </div>
			</div>
			
			<div class="box-body table-responsive">
                <table id="OriginsDestinations" class="table table-bordered table-striped">
					<tbody>
						<tr>		<td><strong><?php echo __('Id'); ?></strong></td>
		<td>
			<?php echo h($originsDestination['OriginsDestination']['id']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Title'); ?></strong></td>
		<td>
			<?php echo h($originsDestination['OriginsDestination']['title']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Color'); ?></strong></td>
		<td>
			<?php echo h($originsDestination['OriginsDestination']['color']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Created'); ?></strong></td>
		<td>
			<?php echo h($originsDestination['OriginsDestination']['created']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Modified'); ?></strong></td>
		<td>
			<?php echo h($originsDestination['OriginsDestination']['modified']); ?>
			&nbsp;
		</td>
</tr>					</tbody>
				</table><!-- /.table table-striped table-bordered -->
			</div><!-- /.table-responsive -->
			
		</div><!-- /.view -->

					
			<div class="box box-primary">
				<div class="box-header">
					<h3 class="box-title"><?php echo __('Related Transactions'); ?></h3>
					<div class="box-tools pull-right">
						<?php echo $this->Html->link('<i class="glyphicon glyphicon-plus"></i> '.__('New Transaction'), array('controller' => 'transactions', 'action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>					</div><!-- /.actions -->
				</div>
				<?php if (!empty($originsDestination['Transaction'])): ?>
					
					<div class="box-body table-responsive">
						<table class="table table-bordered table-striped">
							<thead>
								<tr>
											<th class="text-center"><?php echo __('Id'); ?></th>
		<th class="text-center"><?php echo __('Transactions Type Id'); ?></th>
		<th class="text-center"><?php echo __('Account Id'); ?></th>
		<th class="text-center"><?php echo __('Valor'); ?></th>
		<th class="text-center"><?php echo __('Obs'); ?></th>
		<th class="text-center"><?php echo __('Categories Type Id'); ?></th>
		<th class="text-center"><?php echo __('Origins Destination Id'); ?></th>
		<th class="text-center"><?php echo __('Created'); ?></th>
		<th class="text-center"><?php echo __('Modified'); ?></th>
									<th class="text-center"><?php echo __('Actions'); ?></th>
								</tr>
							</thead>
							<tbody>
									<?php
										$i = 0;
										foreach ($originsDestination['Transaction'] as $transaction): ?>
		<tr>
			<td class="text-center"><?php echo $transaction['id']; ?></td>
			<td class="text-center"><?php echo $transaction['transactions_type_id']; ?></td>
			<td class="text-center"><?php echo $transaction['account_id']; ?></td>
			<td class="text-center"><?php echo $transaction['valor']; ?></td>
			<td class="text-center"><?php echo $transaction['obs']; ?></td>
			<td class="text-center"><?php echo $transaction['categories_type_id']; ?></td>
			<td class="text-center"><?php echo $transaction['origins_destination_id']; ?></td>
			<td class="text-center"><?php echo $transaction['created']; ?></td>
			<td class="text-center"><?php echo $transaction['modified']; ?></td>
			<td class="text-center">
				<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-eye-open"></i>'), array('controller' => 'transactions', 'action' => 'view', $transaction['id']), array('class' => 'btn btn-primary btn-xs', 'escape' => false)); ?>
				<?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i>'), array('controller' => 'transactions', 'action' => 'edit', $transaction['id']), array('class' => 'btn btn-warning btn-xs', 'escape' => false)); ?>
				<?php echo $this->Form->postLink(__('<i class="glyphicon glyphicon-trash"></i>'), array('controller' => 'transactions', 'action' => 'delete', $transaction['id']), array('class' => 'btn btn-danger btn-xs', 'escape' => false), __('Are you sure you want to delete # %s?', $transaction['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
							</tbody>
						</table><!-- /.table table-striped table-bordered -->
					</div><!-- /.table-responsive -->
					
				<?php endif; ?>

				
				
			</div><!-- /.related -->

			
	</div><!-- /#page-content .span9 -->

</div><!-- /#page-container .row-fluid -->
