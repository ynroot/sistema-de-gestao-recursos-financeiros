
<div class="row">
    <div class="col-xs-12">
		
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title"><?php  echo __('Task'); ?></h3>
				<div class="box-tools pull-right">
	                <?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i> Edit'), array('action' => 'edit', $task['Task']['id']), array('class' => 'btn btn-primary', 'escape' => false)); ?>
	            </div>
			</div>
			
			<div class="box-body table-responsive">
                <table id="Tasks" class="table table-bordered table-striped">
					<tbody>
						<tr>		<td><strong><?php echo __('Id'); ?></strong></td>
		<td>
			<?php echo h($task['Task']['id']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Name'); ?></strong></td>
		<td>
			<?php echo h($task['Task']['name']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Perc Concluido'); ?></strong></td>
		<td>
			<?php echo h($task['Task']['perc_concluido']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Descricao'); ?></strong></td>
		<td>
			<?php echo h($task['Task']['descricao']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Created'); ?></strong></td>
		<td>
			<?php echo h($task['Task']['created']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Modified'); ?></strong></td>
		<td>
			<?php echo h($task['Task']['modified']); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Account'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($task['Account']['title'], array('controller' => 'accounts', 'action' => 'view', $task['Account']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr>					</tbody>
				</table><!-- /.table table-striped table-bordered -->
			</div><!-- /.table-responsive -->
			
		</div><!-- /.view -->

			
	</div><!-- /#page-content .span9 -->

</div><!-- /#page-container .row-fluid -->
