<script>
 window.onload = function () {
     
                    var ctx = document.getElementById("chart-area").getContext("2d");
                    window.myPolarArea = new Chart(ctx).PolarArea(polarData, {
                        responsive: true
                    });
                    
                    var ctx1 = document.getElementById("chart-area1").getContext("2d");
                    window.myPolarArea = new Chart(ctx1).PolarArea(polarData1, {
                        responsive: true
                    });
                    
                    var ctx2 = document.getElementById("chart-area2").getContext("2d");
                    window.myPolarArea = new Chart(ctx2).PolarArea(polarData1, {
                        responsive: true
                    });
                    
                    var ctx3 = document.getElementById("chart-area3").getContext("2d");
                    window.myPolarArea = new Chart(ctx3).PolarArea(polarData1, {
                        responsive: true
                    });
                };
                
                
           

</script>

<div class="row">
    <div class="col-lg-12">

        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">Categorias</div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="table-responsive">
                        <div id="canvas-holder" style="width:100%"><!-- canvas -->
                           
                                <canvas id="chart-area" width="300" height="200"/>
                            
                        </div><!-- canvas -->
                    </div>
                </div>
            </div>
            <script>

                var polarData = [
                    {
                        value: 10,
                        color: "#F7464A",
                        highlight: "#FF5A5E",
                        label: "Red"
                    },
                    {
                        value: 25,
                        color: "#46BFBD",
                        highlight: "#5AD3D1",
                        label: "Green"
                    },
                    {
                        value: 40,
                        color: "#FDB45C",
                        highlight: "#FFC870",
                        label: "Yellow"
                    },
                    {
                        value: 19,
                        color: "#949FB1",
                        highlight: "#A8B3C5",
                        label: "Grey"
                    },
                    {
                        value: 12,
                        color: "#4D5360",
                        highlight: "#616774",
                        label: "Dark Grey"
                    }

                ];

               



            </script>

        </div>

        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">Contas</div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="table-responsive">
                        <div id="canvas-holder" style="width:100%"><!-- canvas -->
                           
                                <canvas id="chart-area1" width="300" height="200"/>
                            
                        </div><!-- canvas -->
                    </div>
                </div>
            </div>
            <script>

                var polarData1 = [
                    {
                        value: 30,
                        color: "#F7464A",
                        highlight: "#FF5A5E",
                        label: "Red"
                    },
                    {
                        value: 5,
                        color: "#46BFBD",
                        highlight: "#5AD3D1",
                        label: "Green"
                    },
                    {
                        value: 50,
                        color: "#FDB45C",
                        highlight: "#FFC870",
                        label: "Yellow"
                    },
                    {
                        value: 70,
                        color: "#949FB1",
                        highlight: "#A8B3C5",
                        label: "Grey"
                    },
                    {
                        value: 12,
                        color: "#4D5360",
                        highlight: "#616774",
                        label: "Dark Grey"
                    }

                ];

                



            </script>

        </div>
        
        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">eeee</div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="table-responsive">
                        <div id="canvas-holder" style="width:100%"><!-- canvas -->
                           
                                <canvas id="chart-area2" width="300" height="200"/>
                            
                        </div><!-- canvas -->
                    </div>
                </div>
            </div>
            <script>

                var polarData2 = [
                    {
                        value: 100,
                        color: "#F7464A",
                        highlight: "#FF5A5E",
                        label: "Red"
                    },
                    {
                        value: 30,
                        color: "#46BFBD",
                        highlight: "#5AD3D1",
                        label: "Green"
                    },
                    {
                        value: 50,
                        color: "#FDB45C",
                        highlight: "#FFC870",
                        label: "Yellow"
                    },
                    {
                        value: 40,
                        color: "#949FB1",
                        highlight: "#A8B3C5",
                        label: "Grey"
                    },
                    {
                        value: 12,
                        color: "#4D5360",
                        highlight: "#616774",
                        label: "Dark Grey"
                    }

                ];

                



            </script>

        </div>
        
        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">ppp</div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div class="table-responsive">
                        <div id="canvas-holder" style="width:100%"><!-- canvas -->
                           
                                <canvas id="chart-area3" width="300" height="200"/>
                            
                        </div><!-- canvas -->
                    </div>
                </div>
            </div>
            <script>

                var polarData3 = [
                    {
                        value: 100,
                        color: "#F7464A",
                        highlight: "#FF5A5E",
                        label: "Red"
                    },
                    {
                        value: 30,
                        color: "#46BFBD",
                        highlight: "#5AD3D1",
                        label: "Green"
                    },
                    {
                        value: 50,
                        color: "#FDB45C",
                        highlight: "#FFC870",
                        label: "Yellow"
                    },
                    {
                        value: 40,
                        color: "#949FB1",
                        highlight: "#A8B3C5",
                        label: "Grey"
                    },
                    {
                        value: 12,
                        color: "#4D5360",
                        highlight: "#616774",
                        label: "Dark Grey"
                    }

                ];

                



            </script>

        </div>
    </div>
</div>