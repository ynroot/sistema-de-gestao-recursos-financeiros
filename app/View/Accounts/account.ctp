
        <?php
                    if (isset($_GET['idConta']) && isset($_GET['nomeConta'])):
                        //echo "conta id => ".$_GET['idConta']."  ---------------";
                        //echo "  conta nome => ".$_GET['nomeConta'];
                    $this->set('title_for_layout' , __($_GET['nomeConta']) );
                    endif;
          ?>
<script>
window.onload = function(){

        var ctx = document.getElementById("canvas").getContext("2d");
                    window.myLine = new Chart(ctx).Line(lineChartData, {
                        responsive: true
                    });
    }
</script>
<!--        $this->params['pass']['0'],-->
        <div class="row">
            <div class="col-xs-12">
                <ul class="nav nav-pills" role="tablist">
                    <li role="presentation" class="active"><a>Home</a></li>
                    <li role="presentation">
                        <a href="<?php
                        echo $this->Html->url(
                            array(
                                'controller' => 'tasks',
                                'action' => 'index',$this->request->query['idConta']
                            ));
                        ?>">Tasks</a>
                    </li>
                    <li role="presentation">
                        <a href="<?php
                        echo $this->Html->url(
                            array(
                                'controller' => 'accounts',
                                'action' => 'view',$this->request->query['idConta']
                            ));
                        ?>">Info</a>
                    </li>
                    <li role="presentation">
                        <a href="<?php
                        echo $this->Html->url(
                            array(
                                'controller' => 'zzz',
                                'action' => 'zzz',
                            ));
                        ?>">zzz</a>
                    </li>
                    <li role="presentation">
                        <a href="<?php
                        echo $this->Html->url(
                            array(
                                'controller' => 'xxx',
                                'action' => 'xxx',
                            ));
                        ?>">xxx</a>
                    </li>

                </ul>
            </div>
        </div>
<?php
//debug($this->params);
//echo $this->request->query['nomeConta'];

?>
        <br/>

        <div class="row">
<!-- pagamento -->
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-red">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="glyphicon glyphicon-arrow-up fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">
                            <?php
                            $transactions = $this->requestAction('/accounts/transacao/2/'.$this->request->query['idConta']);
                               echo count($transactions);
                              ?>
                        </div>
                        <div><?php echo __('payments') ?></div>
                    </div>
                </div>
            </div>
            <a href="#">
            <div class="panel-footer">
                <span class="pull-left">View Details</span>
                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                <div class="clearfix"></div>
            </div>
            </a>
        </div>
    </div>
    <!-- deposito -->
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-green">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="glyphicon glyphicon-arrow-down fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">
                            <?php
                            $transactions = $this->requestAction('/accounts/transacao/1/'.$this->request->query['idConta']);
                            echo count($transactions);
                            ?>
                        </div>
                        <div><?php echo __('deposits') ?></div>
                    </div>
                </div>
            </div>
            <a href="#">
            <div class="panel-footer">
                <span class="pull-left">View Details</span>
                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                <div class="clearfix"></div>
            </div>
            </a>
        </div>
    </div>

    <div class="col-lg-3 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-credit-card fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">26</div>
                        <div><?php echo __('investments') ?></div>
                    </div>
                </div>
            </div>
            <a href="#">
            <div class="panel-footer">
                <span class="pull-left">View Details</span>
                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                <div class="clearfix"></div>
            </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-yellow">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-inbox fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">124</div>
                        <div><?php echo __('balance available') ?></div>
                    </div>
                </div>
            </div>
            <a href="#">
            <div class="panel-footer">
                <span class="pull-left">View Details</span>
                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                <div class="clearfix"></div>
            </div>
            </a>
        </div>
    </div>

</div>
        <div class="col-lg-12">
            <div class="progress">
                <div class="progress-bar progress-bar-striped" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%"><span class="sr-only">60% Complete</span></div>
            </div>
        </div><!-- 12 -->
<!--grafico #1-->
<!-- novo -->
<div class="col-lg-12">
    <div class="panel panel-default">
        <div class="panel-heading">legenda</div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div class="table-responsive">
                <div style="width:100%"><!-- canvas -->
                <div>
                    <canvas id="canvas" height="200" width="600"></canvas>
                </div>
                </div><!-- canvas -->
            </div>
        </div>
    </div>
    <script>
    var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
    var lineChartData = {
    labels : ["Aluminio","dobradisas","compasos","esquadros","parafusos","xxx","ddd","eeee","rrrr","hhhh","wwww","vvvv"],
    datasets : [
    {
    label: "Deposito",
                                    fillColor : "rgba(156, 230, 156, 1)",
                                    strokeColor : "rgba(55, 186, 55, 1)",
                                    pointColor : "rgba(14, 230, 14, 1)",
                                    pointStrokeColor : "#fff",
                                    pointHighlightFill : "#fff",
                                    pointHighlightStroke : "rgba(220,220,220,1)",
    data : [10, 5, 8, 8, 56, 5, 40, 80, 41, 56, 55, 40]
    },
    {
    label: "Pagamento",
                                    fillColor : "rgba(129, 89, 89, 0.40)",
                                    strokeColor : "rgba(239, 70, 67, 1)",
                                    pointColor : "rgba(251, 8, 8, 1)",
                                    pointStrokeColor : "#fff",
                                    pointHighlightFill : "#fff",
                                    pointHighlightStroke : "rgba(151,187,205,1)",
    data : [10, 9, 8, 8, 6, 5, 40, 0, 1, 6, 5, 4]
    }
                                    ,
    {
    label: "Patrimonio",
                                    fillColor : "rgba(52, 59, 209, 0.40)",
                                    strokeColor : "rgba(151,187,205,1)",
                                    pointColor : "rgba(8, 28, 251, 1)",
                                    pointStrokeColor : "#fff",
                                    pointHighlightFill : "#fff",
                                    pointHighlightStroke : "rgba(151,187,205,1)",
    data : [6, 9, 18, 1, 16, 15, 4, 80, 8, 56, 5, 3]
    }
    ]
    }

    </script>
</div>

<!-- /novo -->

    <?php
    echo $this->Html->script('chart');

    ?>