<?php echo $this->Html->css('datatables/dataTables.bootstrap'); ?>
<div class="row">
    <div class="col-xs-12">

        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"><?php echo __('Accounts'); ?></h3>
                <div class="box-tools pull-right">
                    <?php echo $this->Html->link(__('<i class="glyphicon glyphicon-plus"></i> New Account'), array('action' => 'add'), array('class' => 'btn btn-primary', 'escape' => false)); ?>
                </div>
            </div>
            <div class="box-body table-responsive">
                <table id="Accounts" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th class="text-center"><?php echo $this->Paginator->sort('id'); ?></th>
                        <th class="text-center"><?php echo $this->Paginator->sort('title'); ?></th>
                        <th class="text-center"><?php echo $this->Paginator->sort('initial_value'); ?></th>
                        <th class="text-center"><?php echo $this->Paginator->sort('saldo'); ?></th>
                        <th class="text-center"><?php echo $this->Paginator->sort('dif_transacao'); ?></th>

                        <th class="text-center"><?php echo $this->Paginator->sort('created'); ?></th>
                        <th class="text-center"><?php echo $this->Paginator->sort('modified'); ?></th>
                        <th class="text-center"><?php echo $this->Paginator->sort('client_id'); ?></th>
                        <th class="text-center"><?php echo __('Actions'); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($accounts as $account): ?>
                        <tr>
                            <td class="text-center"><?php echo h($account['Account']['id']); ?>&nbsp;</td>
                            <td class="text-center"><?php echo h($account['Account']['title']); ?>&nbsp;</td>
                            <td class="text-center"><?php echo h($account['Account']['initial_value']); ?>&nbsp;</td>
                            <td class="text-center"><?php echo h($account['Account']['saldo']); ?>&nbsp;</td>
                            <td class="text-center"><?php echo h($account['Account']['dif_transacao']); ?>&nbsp;</td>

                            <td class="text-center"><?php echo h($account['Account']['created']); ?>&nbsp;</td>
                            <td class="text-center"><?php echo h($account['Account']['modified']); ?>&nbsp;</td>
                            <td class="text-center"><?php echo h($account['Client']['firstname'].' '.$account['Client']['lastname']); ?>&nbsp;</td>
                            <td class="text-center">
                                <?php echo $this->Html->link(__('<i class="glyphicon glyphicon-eye-open"></i>'), array('action' => 'view', $account['Account']['id']), array('class' => 'btn btn-primary btn-xs', 'escape' => false)); ?>
                                <?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i>'), array('action' => 'edit', $account['Account']['id']), array('class' => 'btn btn-warning btn-xs', 'escape' => false)); ?>
                                <?php echo $this->Form->postLink(__('<i class="glyphicon glyphicon-trash"></i>'), array('action' => 'delete', $account['Account']['id']), array('class' => 'btn btn-danger btn-xs', 'escape' => false), __('Are you sure you want to delete # %s?', $account['Account']['id'])); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div><!-- /.table-responsive -->


        </div><!-- /.index -->

    </div><!-- /#page-content .col-sm-9 -->

</div><!-- /#page-container .row-fluid -->

<?php
echo $this->Html->script('jquery.min');
echo $this->Html->script('plugins/datatables/jquery.dataTables');
echo $this->Html->script('plugins/datatables/dataTables.bootstrap');
?>
<script type="text/javascript">
    $(function() {
        $("#Accounts").dataTable();
    });
</script>