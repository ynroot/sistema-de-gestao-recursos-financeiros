
<div class="row">
    <div class="col-xs-12">
		
		<div class="box box-primary">
			<div class="box-header">
				<h3 class="box-title"><?php  echo __('Accounts Has Budget'); ?></h3>
				<div class="box-tools pull-right">
	                <?php echo $this->Html->link(__('<i class="glyphicon glyphicon-pencil"></i> Edit'), array('action' => 'edit', $accountsHasBudget['AccountsHasBudget']['id']), array('class' => 'btn btn-primary', 'escape' => false)); ?>
	            </div>
			</div>
			
			<div class="box-body table-responsive">
                <table id="AccountsHasBudgets" class="table table-bordered table-striped">
					<tbody>
						<tr>		<td><strong><?php echo __('Account'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($accountsHasBudget['Account']['title'], array('controller' => 'accounts', 'action' => 'view', $accountsHasBudget['Account']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr><tr>		<td><strong><?php echo __('Budget'); ?></strong></td>
		<td>
			<?php echo $this->Html->link($accountsHasBudget['Budget']['id'], array('controller' => 'budgets', 'action' => 'view', $accountsHasBudget['Budget']['id']), array('class' => '')); ?>
			&nbsp;
		</td>
</tr>					</tbody>
				</table><!-- /.table table-striped table-bordered -->
			</div><!-- /.table-responsive -->
			
		</div><!-- /.view -->

			
	</div><!-- /#page-content .span9 -->

</div><!-- /#page-container .row-fluid -->
