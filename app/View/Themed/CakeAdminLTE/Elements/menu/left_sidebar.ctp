
<!-- Left side column. contains the logo and sidebar -->
<aside class="left-side sidebar-offcanvas">                
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        
     
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
         
            <?php echo $this->element('listAccount/listAccount'); ?>

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-pencil-square-o"></i>
                    <span><?php echo __('Transactions') ?></span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="<?php echo $this->Html->url(array(
                            'controller'=>'transactions',
                            'action'=>'index',
                        )); ?>">
                            <i class="fa fa-angle-double-right"></i>
                             <?php echo __('Transactions list') ?>
                         </a>
                     </li>
                        <li>
                        <a href="<?php echo $this->Html->url(array(
                            'controller'=>'transactions',
                            'action'=>'add',
                        )); ?>">
                            <i class="fa fa-angle-double-right"></i>
                             <?php echo __('Add Transactions') ?>
                         </a>
                     </li>
                     <li>
                        <a href="<?php echo $this->Html->url(array(
                            'controller'=>'transactions_types',
                            'action'=>'index',
                        )); ?>">
                            <i class="fa fa-angle-double-right"></i>
                             <?php echo __('categories types list') ?>
                         </a>
                     </li>
                        <li>
                        <a href="<?php echo $this->Html->url(array(
                            'controller'=>'categories_types',
                            'action'=>'add',
                        )); ?>">
                            <i class="fa fa-angle-double-right"></i>
                             <?php echo __('Add categoriestypes') ?>
                         </a>
                     </li>
                    
                </ul>
            </li>
            

            <li class="treeview">
                <a href="#">
                    <i class="fa fa-pencil-square-o"></i>
                    <span><?php echo __('Investments') ?></span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="<?php echo $this->Html->url(array(
                            'controller'=>'investments',
                            'action'=>'index',
                        )); ?>">
                            <i class="fa fa-angle-double-right"></i>
                             <?php echo __('Investment list') ?>
                         </a>
                     </li>
                        <li>
                        <a href="<?php echo $this->Html->url(array(
                            'controller'=>'investments',
                            'action'=>'add',
                        )); ?>">
                            <i class="fa fa-angle-double-right"></i>
                             <?php echo __('Add investments') ?>
                         </a>
                     </li>
                    
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-pencil-square-o"></i>
                    <span><?php echo __('Account') ?></span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="<?php echo $this->Html->url(array(
                            'controller'=>'accounts',
                            'action'=>'conta',
                        )); ?>">
                            <i class="fa fa-angle-double-right"></i>
                             <?php echo __('Account list') ?>
                         </a>
                     </li>
                        <li>
                        <a href="<?php echo $this->Html->url(array(
                            'controller'=>'accounts',
                            'action'=>'add',
                        )); ?>">
                            <i class="fa fa-angle-double-right"></i>
                             <?php echo __('Add acounts') ?>
                         </a>
                     </li>
                    
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-pencil-square-o"></i>
                    <span><?php echo __('budgets') ?></span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="<?php echo $this->Html->url(array(
                            'controller'=>'budgets',
                            'action'=>'index',
                        )); ?>">
                            <i class="fa fa-angle-double-right"></i>
                             <?php echo __('budgets list') ?>
                         </a>
                     </li>
                        <li>
                        <a href="<?php echo $this->Html->url(array(
                            'controller'=>'budgets',
                            'action'=>'add',
                        )); ?>">
                            <i class="fa fa-angle-double-right"></i>
                             <?php echo __('Add budgets') ?>
                         </a>
                     </li>

                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-pencil-square-o"></i>
                    <span><?php echo __('categories types') ?></span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="<?php echo $this->Html->url(array(
                            'controller'=>'categories_types',
                            'action'=>'index',
                        )); ?>">
                            <i class="fa fa-angle-double-right"></i>
                             <?php echo __('categories types list') ?>
                         </a>
                     </li>
                        <li>
                        <a href="<?php echo $this->Html->url(array(
                            'controller'=>'categories_types',
                            'action'=>'add',
                        )); ?>">
                            <i class="fa fa-angle-double-right"></i>
                             <?php echo __('Add categories types') ?>
                         </a>
                     </li>
                    
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-pencil-square-o"></i>
                    <span><?php echo __('Origins/Destinations') ?></span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="<?php echo $this->Html->url(array(
                            'controller'=>'origins_destinations',
                            'action'=>'index',
                        )); ?>">
                            <i class="fa fa-angle-double-right"></i>
                             <?php echo __('Origins/Destinations list') ?>
                         </a>
                     </li>
                        <li>
                        <a href="<?php echo $this->Html->url(array(
                            'controller'=>'origins_destinations',
                            'action'=>'add',
                        )); ?>">
                            <i class="fa fa-angle-double-right"></i>
                             <?php echo __('Add Origins/Destinations') ?>
                         </a>
                     </li>
                    
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-bar-chart-o"></i>
                    <span><?php echo __('Graficos') ?></span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li>
                        <a href="<?php echo $this->Html->url(array(
                            'controller'=>'accounts',
                            'action'=>'recursosin',
                        )); ?>">
                            <i class="fa fa-angle-double-right"></i>
                             <?php echo __('Entrada de recursos') ?>
                         </a>
                     </li>
                         <li>
                        <a href="<?php echo $this->Html->url(array(
                            'controller'=>'accounts',
                            'action'=>'recursosout',
                        )); ?>">
                            <i class="fa fa-angle-double-right"></i>
                             <?php echo __('Saida de recursos') ?>
                         </a>
                     </li>
                    
                </ul>
            </li>
            <li>
                        <a href="<?php echo $this->Html->url(array(
                            'controller'=>'users',
                            'action'=>'calendar',
                        )); ?>">
                            <i class="fa fa-angle-double-right"></i>
                             <?php echo __('calendario') ?>
                         </a>
                     </li>
<!--            <li>
                <a href="pages/mailbox.html">
                    <i class="fa fa-envelope"></i> <span>Mailbox</span>
                    <small class="badge pull-right bg-yellow">12</small>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-folder"></i> <span>Examples</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="pages/examples/invoice.html"><i class="fa fa-angle-double-right"></i> Invoice</a></li>
                    <li><a href="pages/examples/login.html"><i class="fa fa-angle-double-right"></i> Login</a></li>
                    <li><a href="pages/examples/register.html"><i class="fa fa-angle-double-right"></i> Register</a></li>
                    <li><a href="pages/examples/lockscreen.html"><i class="fa fa-angle-double-right"></i> Lockscreen</a></li>
                    <li><a href="pages/examples/404.html"><i class="fa fa-angle-double-right"></i> 404 Error</a></li>
                    <li><a href="pages/examples/500.html"><i class="fa fa-angle-double-right"></i> 500 Error</a></li>                                
                    <li><a href="pages/examples/blank.html"><i class="fa fa-angle-double-right"></i> Blank Page</a></li>
                </ul>
            </li>-->
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>