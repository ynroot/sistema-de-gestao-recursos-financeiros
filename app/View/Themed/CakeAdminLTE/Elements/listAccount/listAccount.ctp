   <li class="active">
                <?= $this->Html->link('<i class="fa fa-dashboard"></i> <span> Active acount</span>', '#', array('escape' => false)); ?>
            </li>

            <?php 
            $accounts = $this->requestAction('accounts/userAccounts/');
            foreach ($accounts as  $account): ?>
            <li>
                <a href="<?php echo $this->Html->url(
                    array(
                    'controller'=>'accounts',
                    'action'=>'account',$account['Account']['id'],
                    '?'=>array(
                            'idConta'=>$account['Account']['id'],
                            'nomeConta'=>$account['Account']['title'],
                            ))); ?>">
                    <i class="fa fa-th bg-red"></i> 
                    <span><?php echo $account['Account']['title']; ?></span> 

                </a>
            </li>

        <?php endforeach; ?>